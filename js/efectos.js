$(document).ready(function () {
    // $('html, body').animate({
    //     scrollTop: 0
    // }, 500);

    // Efecto menú
    $('.menu a').each(function (index, element) {
        $(this).css({
            'top': '-200px'
        });
        $(this).animate({
            'top': 0
        }, 500 + (500 * index))
    });

    // Efecto Header
    if ($(window).width() > 800) {
        $('header .textos').css({
            opacity: 0,
            marginTop: 0
        });

        $('header .textos').animate({
            opacity: 1,
            marginTop: '-52px'
        }, 1500);
    }

    // scroll elementos menú
    var acercaDe = $('#acerca-de').offset().top;
    $('#btn-acerca-de').on('click', function (e) {
        e.preventDefault();
        $('html, body').animate({
            scrollTop: acercaDe
        }, 500);
    });

    var menu = $('#platillos').offset().top;
    $('#btn-menu').on('click', function (e) {
        e.preventDefault();
        $('html, body').animate({
            scrollTop: menu
        }, 500);
    });

    var galeria = $('#galeria').offset().top;
    $('#btn-galeria').on('click', function (e) {
        e.preventDefault();
        $('html, body').animate({
            scrollTop: galeria
        }, 500);
    });
    var ubicacion = $('#ubicacion').offset().top;
    $('#btn-ubicacion').on('click', function (e) {
        e.preventDefault();
        $('html, body').animate({
            scrollTop: ubicacion
        }, 500);
    });
});