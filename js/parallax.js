$(document).ready(function () {
    $(window).scroll(function () {
        var windowWidth = $(window).width();
        if (windowWidth > 800) {
            var scroll = $(window).scrollTop();

            $('header .textos').css({
                'transform': 'translate(0px,' + scroll / 2 + '%)'
            });

            $('.acerca-de article').css({
                'transform': 'translate(0px, -' + (scroll / 4) + '%)'
            });
        }
    });

    // ponemos la posición del acerca de por si es una tableta y se pone vertical
    $(window).resize(function () {
        var windowWidth = $(window).width();
        if (windowWidth < 800) {
            $('.acerca-de article').css({
                'transform': 'translate(0px, 0px)'
            });
        }
    });
});